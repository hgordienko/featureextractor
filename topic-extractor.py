#!/usr/bin/env python

#from __future__ import print_function
from sys import argv, stderr
from os import path
from libshorttext.classifier import *

liblinear_arguments = ''
extra_svm_files     = []

data = 'temp.txt'
myfile = open('temp.txt', 'w')
myfile.write('\t' + argv[1])
myfile.close()
model = '/home/lego/masters-work/lib/libshorttext/topics/topics-trainig.txt.model/'

m = TextModel()
m.load(model)
predict_result = predict_text(data, m, svm_file=None, predict_arguments = liblinear_arguments, extra_svm_files = extra_svm_files)

res = ",".join(predict_result.predicted_y)
print(res)
#print(zip(m.get_labels,predict_result.decvals))

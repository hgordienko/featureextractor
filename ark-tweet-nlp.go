//package featureExtractors
package main

import (
	"bytes"
	"log"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
)

/**
http://www.cs.cmu.edu/~ark/TweetNLP/
Java-based tokenizer and part-of-speech tagger for tweets,
its training data of manually labeled POS annotated tweets,
a web-based annotation tool, and hierarchical word clusters from unlabeled tweets

// output - word, tag, confidence ( Conference on Natural Language Learning format )
// http://conll.cemantix.org/2011/data.html
*/
type Float64 float64

type TokenType string

const (
	Adjective            = "A"
	Noun                 = "N"
	Emoticon             = "E"
	ProperNoun           = "^"
	Interjection         = "!"
	Misc                 = "G"
	Number               = "$"
	Punctuation          = ","
	Verb                 = "V"
	Preposition          = "P"
	Pronoun              = "O"
	Determiner           = "D"
	Mention              = "@"
	Continuation         = "~"
	Hashtag              = "#"
	URL                  = "U"
	NominalPossessive    = "S"
	ProperNounPossessive = "Z"
	NominalVerbal        = "L"
	ProperNounVerbal     = "M"
	xVerbal              = "Y"
	Adverb               = "R"
	CoordConjunction     = "&"
	Predeterminers       = "X"
	VerbParticle         = "T"
)

type arkTweetNlp struct {
	tokensTotal int
	// token type : number of such tokens
	tokensCount map[string]float64
	threshold   Float64 // confidence threshold - filter tokens with less confidence
	precision   Float64 // precision for comparing floats
}

func (number Float64) isLess(number2 Float64, precision Float64) bool {
	difference := number2 - number
	if difference > 0 {
		return difference > precision
	}

	return false
}

func (featureExtr *arkTweetNlp) Extract(data RawData) <-chan FeatureSet {
	out := make(chan FeatureSet)
	go func() {
		// fmt.Printf("ARK_TWEET_NLP start processing @%s...\n", data.ScreenName)
		for _, tweet := range data.Tweets {
			//do not analyze retweets
			if tweet.RetweetedStatus == nil {
				featureExtr.execCommand(tweet.Text)
			}
		}

		// fmt.Printf("BEFORE NORM:\n 		total=%d \n", featureExtr.tokensTotal)
		// normalize values
		for key, value := range featureExtr.tokensCount {
			// fmt.Printf(" 		%s=%.2f \n", key, featureExtr.tokensCount[key])
			featureExtr.tokensCount[key] = (value / float64(featureExtr.tokensTotal))
			// fmt.Printf("%s %.2f\n", key, featureExtr.tokensCount[key])
		}

		// fmt.Printf("%s processed, %d tokens found \n",
		// 	data.ScreenName,
		// 	featureExtr.tokensTotal)

		featureSet := FeatureSet{data.ScreenName, featureExtr.tokensCount}
		out <- featureSet
		close(out)
	}()
	return out
}

func NewArkTweetNlp() *arkTweetNlp {
	featureExtr := arkTweetNlp{}
	featureExtr.tokensTotal = 0
	featureExtr.tokensCount = map[string]float64{
		Adjective:            0,
		Noun:                 0,
		Emoticon:             0,
		ProperNoun:           0,
		Interjection:         0,
		Misc:                 0,
		Number:               0,
		Punctuation:          0,
		Verb:                 0,
		Preposition:          0,
		Pronoun:              0,
		Determiner:           0,
		Mention:              0,
		Continuation:         0,
		Hashtag:              0,
		URL:                  0,
		NominalPossessive:    0,
		ProperNounPossessive: 0,
		NominalVerbal:        0,
		ProperNounVerbal:     0,
		xVerbal:              0,
		Adverb:               0,
		CoordConjunction:     0,
		Predeterminers:       0,
		VerbParticle:         0,
	}
	featureExtr.precision = 0.00001
	featureExtr.threshold = 0.80000
	return &featureExtr
}

func (featureExtr *arkTweetNlp) execCommand(tweetText string) {
	// TODO: add path to lib properly
	cmd := exec.Command("/home/lego/Go/bin/ark-tweet-nlp/runTagger.sh", "--output-format", "conll")
	//	fmt.Printf("tweet text:\n%s\n", tweetText)
	cmd.Stdin = strings.NewReader(tweetText)
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}

	featureExtr.parseResult(out.String())
}

func (featureExtr *arkTweetNlp) parseResult(result string) {
	//	fmt.Printf("parse result:\n%s\n", result)
	output := regexp.MustCompile("\n").Split(result, -1)
	for _, str := range output {
		// token, token type, confidence
		tokenInfo := regexp.MustCompile("\t").Split(str, 3)
		// fmt.Printf("token info length:\n%d\n", len(tokenInfo))
		if len(tokenInfo) != 3 {
			//	fmt.Printf("invalid format: %s \n", str)
			return
		}

		confidenceString := strings.Replace(tokenInfo[2], ",", ".", -1)

		conf, err := strconv.ParseFloat(confidenceString, 64)
		if err != nil {
			log.Fatal(err)
		}

		confidence := Float64(conf)
		// fmt.Printf("CONFIDENCE %.2f\n", conf)

		if !confidence.isLess(featureExtr.threshold, featureExtr.precision) {
			featureExtr.tokensCount[tokenInfo[1]] += 1
		}
		featureExtr.tokensTotal += 1
		// fmt.Printf("TOKENS TOTAL %d\n", featureExtr.tokensTotal)
	}
}

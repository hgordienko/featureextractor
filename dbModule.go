package main

import (
	_ "fmt"

	"github.com/ChimeraCoder/anaconda"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	DBsession *mgo.Session
)

func fetchTweetsFor(screenName string, session *mgo.Session, number int) []anaconda.Tweet {
	c := session.DB("TwitterData").C("A" + screenName)

	var tweets []anaconda.Tweet

	//err := c.Find(bson.M{"user.screenname": screenName}).All(&tweets)

	err := c.Find(bson.M{}).All(&tweets)
	if err != nil {
		PanicAttack(err)
	}

	//fmt.Printf("%d \n", len(tweets))
	if len(tweets) >= number {
		return tweets[:number]
	}

	return tweets
}

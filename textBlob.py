#!/usr/bin/python

import sys
from textblob import TextBlob
from profanity import profanity

tweet = TextBlob(unicode(sys.argv[1], "utf-8"))

# TODO: remove urls
#print tweet
sentenceCount = len(tweet.sentences)
#print sentenceCount

positive = 0
negative = 0
subjectivity = 0

wordsAverage = 0
swearWordsAverage = 0
upperCasedWords = 0

for sentence in tweet.sentences:
    #print sentence
    wordsAverage = wordsAverage + len(sentence.words)

    for word in sentence.words:
        upperCasedWords = upperCasedWords + word.isupper()
        swearWordsAverage = swearWordsAverage + profanity.contains_profanity(word.strip())

    sentencePolarity = sentence.sentiment.polarity
    sentenceSubjectivity = sentence.sentiment.subjectivity

    if sentencePolarity > 0:
        positive = positive + sentencePolarity
    else:
        negative = negative + sentencePolarity

    # objectivity <-- [0,1] --> subjectivity
    subjectivity = subjectivity + sentenceSubjectivity

    #print ",".join(map(str, [positive, negative, subjectivity, swearWordsAverage, upperCasedWords, wordsAverage])) 

positive = positive / sentenceCount
negative = negative / sentenceCount
subjectivity = subjectivity / sentenceCount
swearWordsAverage = swearWordsAverage * 1.0 / wordsAverage
upperCasedWords = upperCasedWords * 1.0 / wordsAverage
averageSentenceLength = wordsAverage / sentenceCount

result = [positive,negative,subjectivity,swearWordsAverage,upperCasedWords,averageSentenceLength]
resultString = ",".join(map(str, result))
print resultString

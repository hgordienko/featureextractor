package main

import (
	"fmt"
	"math"
	"strconv"
)

const (
	Followers   = "Followers"
	Friends     = "Friends"
	Verified    = "Verified" // boolean
	ListedCount = "ListedCount"
	UtcOffset   = "UtcOffset"
	Favorites   = "Favorites"
	TotalTweets = "TotalTweets"
	GeoEnabled  = "GeoEnabled"
	Favorited   = "FavCount"  // total number of likes from other users
	Quotes      = "Quotes"    // total number of quotes by the user
	Retweeted   = "Retweeted" // total number of retweets by other users
	Hour        = "Hour"      // total number of retweets by other users
)

type userFeatures struct {
	features         map[string]float64
	tweetHours       map[string]float64
	utcOffsetInHours int
}

func keyForHour(hour int) string {
	return Hour + strconv.Itoa(hour)
}

func NewUserFeatures() *userFeatures {
	featureExtr := userFeatures{}
	featureExtr.features = map[string]float64{
		Followers:   0,
		Friends:     0,
		Verified:    0,
		ListedCount: 0,
		UtcOffset:   0,
		Favorites:   0,
		TotalTweets: 0,
		GeoEnabled:  0,
		Favorited:   0,
		Quotes:      0,
		Retweeted:   0,
	}

	featureExtr.tweetHours = make(map[string]float64)
	for i := 0; i < 24; i++ {
		key := keyForHour(i)
		featureExtr.tweetHours[key] = 0
	}

	featureExtr.utcOffsetInHours = 0

	return &featureExtr
}

func (featureExtr *userFeatures) normalize(factor int) {
	floatFactor := float64(factor)
	featureExtr.features[Favorited] = (featureExtr.features[Favorited] / floatFactor)
	featureExtr.features[Retweeted] = (featureExtr.features[Retweeted] / floatFactor)
	featureExtr.features[Quotes] = (featureExtr.features[Quotes] / floatFactor)
}

func (featureExtr *userFeatures) Extract(data RawData) <-chan FeatureSet {
	out := make(chan FeatureSet)

	go func() {
		if len(data.Tweets) != 0 {
			user := data.Tweets[0].User
			featureExtr.features[Followers] = float64(user.FollowersCount)
			featureExtr.features[Friends] = float64(user.FriendsCount)
			if user.GeoEnabled {
				featureExtr.features[GeoEnabled] = 1
			}

			offset := math.Floor(float64(user.UtcOffset) / 3600)
			featureExtr.utcOffsetInHours = int(offset)
			featureExtr.features[UtcOffset] = offset

			if user.Verified {
				featureExtr.features[Verified] = 1
			}

			featureExtr.features[ListedCount] = float64(user.ListedCount)
			featureExtr.features[Favorites] = float64(user.FavouritesCount)
			featureExtr.features[TotalTweets] = float64(user.StatusesCount)
		}

		// fmt.Printf("SIMPLE_FEATURES: start processing @%s...\n", data.ScreenName)
		for _, tweet := range data.Tweets {
			// fmt.Printf("TEXT\n%s \n", tweet.Text)
			// if user is tweet's author
			if tweet.RetweetedStatus == nil {
				featureExtr.features[Retweeted] += float64(tweet.RetweetCount)
				featureExtr.features[Favorited] += float64(tweet.FavoriteCount)

				if tweet.QuotedStatus != nil {
					featureExtr.features[Quotes] += 1
				}
			}

			// Time
			createdAt, _ := tweet.CreatedAtTime()

			name, offset := createdAt.Zone()
			hour := (createdAt.Hour() + featureExtr.utcOffsetInHours) % 24
			if hour < 0 {
				hour = hour + 24
			}

			featureExtr.tweetHours[keyForHour(hour)] += 1
			fmt.Printf("time: %d (zone: %s offset: %d)\n",
				hour,
				name,
				offset)

		}
		// concatenate maps
		for k, v := range featureExtr.tweetHours {
			featureExtr.features[k] = v
		}
		// Normalize
		featureExtr.normalize(len(data.Tweets))

		// fmt.Printf("%s processed, %d tweets \n",
		// 	data.ScreenName,
		// 	len(data.Tweets))

		featureSet := FeatureSet{data.ScreenName, featureExtr.features}
		out <- featureSet
		close(out)
	}()
	return out
}

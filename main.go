package main

// TODO:
// 3) foreach tweet perform feature extraction
// 4) combine results for all tweets ( sum and normalize )
// 5) write features for screen_name to csv file (screen_name.csv)
import (
	"fmt"
	"os"
	"strconv"
	_ "time"

	"gopkg.in/mgo.v2"
)

func main() {
	//workTime := time.Minute
	server := "127.0.0.1:27017" //os.Args[2]
	//dbName := "TwitterData" //os.Args[3]
	if len(os.Args) < 3 {
		fmt.Println("Usage: <path-to-file-with-screen-names> <tweets-number>")
		return
	}

	inputFile := os.Args[1]
	number, err := strconv.Atoi(os.Args[2])

	if err != nil {
		number = 50
		fmt.Printf("Cannot parse param <tweets-number>.\n")
		fmt.Printf("Number of tweets to analyze set to default value: %d\n", number)
	}

	screenNames := FileInput(inputFile)

	DBsession, err := mgo.Dial(server)

	if err != nil {
		fmt.Printf("Error connecting to db server %s.\nError message: %s \n", server, err)
	}

	// fmt.Printf("%t\n", DBsession == nil)
	PanicAttack(err)
	defer DBsession.Close()

	DBsession.SetMode(mgo.Monotonic, true)
	//data := make(chan FeatureSet)

	for _, screenName := range screenNames {
		tweets := fetchTweetsFor(screenName, DBsession, number)
		fmt.Printf("TWEETS COUNT FOR %s: %d \n", screenName, len(tweets))
		rawData := RawData{screenName, tweets}
		featureExtractors := [...]IFeatureExtractor{
			NewSimpleFeatures(),
			NewArkTweetNlp(),
			NewTextBlob(),
			NewUserFeatures(),
			NewTopicExtractor(),
		}

		outputChannels := []<-chan FeatureSet{
			make(chan FeatureSet),
			make(chan FeatureSet),
			make(chan FeatureSet),
			make(chan FeatureSet),
			make(chan FeatureSet),
		}

		for i, _ := range featureExtractors {
			outputChannels[i] = featureExtractors[i].Extract(rawData)
		}

		features := make(map[string]float64)

		for outputData := range merge(outputChannels) {
			fmt.Printf("%s: added %d entries\n", screenName, len(outputData.Features))
			for key, value := range outputData.Features {
				features[key] = value
			}
		}
		// order of fields being written to output is lexicographic
		go FileOutput(screenName, FeatureSet{screenName, features})
	}

	fmt.Printf("\n\nPress any key to exit...")

	// wait for user to exit
	var input string
	fmt.Scanln(&input)
	fmt.Println("done")
}

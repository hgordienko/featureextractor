package main

import (
	"bytes"
	"fmt"
	"log"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
)

/**
https://textblob.readthedocs.io/en/dev/
TextBlob - TextBlob is a Python (2 and 3) library for processing textual data.
 It provides a simple API for diving into common natural language processing
 (NLP) tasks such as part-of-speech tagging, noun phrase extraction,
 sentiment analysis, classification, translation, and more.
*/
const (
	Negative        = "NEG"
	Positive        = "POS"
	Subjectivity    = "SUB"
	SwearWords      = "SWEAR"
	UpperCasedWords = "CAPS"
	SentenceLength  = "SL"
)

type textBlob struct {
	sentiment   map[string]float64
	tweetsTotal int
}

func NewTextBlob() *textBlob {
	featureExtr := textBlob{}
	featureExtr.sentiment = map[string]float64{
		Negative:        0,
		Positive:        0,
		Subjectivity:    0,
		SwearWords:      0,
		UpperCasedWords: 0,
		SentenceLength:  0,
	}
	featureExtr.tweetsTotal = 0

	return &featureExtr
}

func (featureExtr *textBlob) Extract(data RawData) <-chan FeatureSet {
	out := make(chan FeatureSet)
	go func() {
		for _, tweet := range data.Tweets {
			featureExtr.execCommand(tweet.Text)
		}

		// fmt.Printf("BEFORE NORM:\n 		total=%d \n", featureExtr.tokensTotal)
		// normalize values
		for key, value := range featureExtr.sentiment {
			featureExtr.sentiment[key] = (value / float64(featureExtr.tweetsTotal))
		}

		featureSet := FeatureSet{data.ScreenName, featureExtr.sentiment}
		out <- featureSet
		close(out)
	}()
	return out
}

func (featureExtr *textBlob) execCommand(tweetText string) {
	// TODO: add path to lib properly
	cmd := exec.Command("/home/lego/Go/src/bitbucket.org/hgordienko/featureExtractor/textBlob.py", tweetText)
	fmt.Printf("tweet text:\n%s\n", tweetText)
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}

	featureExtr.parseResult(out.String())
}

func (featureExtr *textBlob) parseResult(result string) {
	// TODO: parse positive, negative, subjectivity, sentence length
	fmt.Printf("RESULT:%s\n", result)
	output := regexp.MustCompile(",").Split(strings.TrimSpace(result), -1)
	var figures []float64 = []float64{0, 0, 0, 0, 0, 0}

	for i, str := range output {
		//fmt.Printf("|%s|\n", str)
		val, err := strconv.ParseFloat(str, 64)

		if err != nil {
			log.Fatal(err)
		}

		figures[i] = val
	}

	featureExtr.sentiment[Positive] += figures[0]
	featureExtr.sentiment[Negative] += figures[1]
	featureExtr.sentiment[Subjectivity] += figures[2]
	featureExtr.sentiment[SwearWords] += figures[3]
	featureExtr.sentiment[UpperCasedWords] += figures[4]
	featureExtr.sentiment[SentenceLength] += figures[5]
	featureExtr.tweetsTotal += 1
}

package main

import (
	"bytes"
	"log"
	"os/exec"
	"regexp"
	"strings"
)

// lib-short-text
const (
	Politics       = "POLITICS"
	Sport          = "SPORT"
	Health         = "HEALTH"
	Job            = "JOB"
	Business       = "BUSINESS"
	PopCulture     = "POPCULTURE"
	Relationship   = "RELATIONSHIP"
	ScienceAndTech = "SCIENCETECH"
	Education      = "EDUCATION"
	Crime          = "CRIME"
	Fashion        = "FASHION"
	Food           = "FOOD"
)

type topicExtractor struct {
	topics      map[string]float64
	tweetsTotal int
}

func NewTopicExtractor() *topicExtractor {
	featureExtr := topicExtractor{}
	featureExtr.topics = map[string]float64{
		Politics:       0,
		Sport:          0,
		Health:         0,
		Job:            0,
		Business:       0,
		PopCulture:     0,
		Relationship:   0,
		ScienceAndTech: 0,
		Education:      0,
		Crime:          0,
		Fashion:        0,
		Food:           0,
	}
	featureExtr.tweetsTotal = 0

	return &featureExtr
}

func (featureExtr *topicExtractor) Extract(data RawData) <-chan FeatureSet {
	out := make(chan FeatureSet)
	go func() {
		for _, tweet := range data.Tweets {
			featureExtr.execCommand(tweet.Text)
		}

		// fmt.Printf("BEFORE NORM:\n 		total=%d \n", featureExtr.tokensTotal)
		// normalize values
		for key, value := range featureExtr.topics {
			featureExtr.topics[key] = (value / float64(featureExtr.tweetsTotal))
		}

		featureSet := FeatureSet{data.ScreenName, featureExtr.topics}
		out <- featureSet
		close(out)
	}()
	return out
}

func (featureExtr *topicExtractor) execCommand(tweetText string) {
	// TODO: add path to lib properly
	cmd := exec.Command("/home/lego/Go/src/bitbucket.org/hgordienko/featureExtractor/topic-extractor.py", tweetText)
	// fmt.Printf("tweet text:\n%s\n", tweetText)
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}

	featureExtr.parseResult(out.String())
}

func (featureExtr *topicExtractor) parseResult(result string) {
	// fmt.Printf("%s\n", result)
	output := regexp.MustCompile(",").Split(strings.TrimSpace(result), -1)

	for _, str := range output {
		// 	fmt.Printf("|%s|\n", str)
		featureExtr.topics[str] += 1
		// fmt.Printf("%s\n", output[1])
	}

	featureExtr.tweetsTotal += 1
}

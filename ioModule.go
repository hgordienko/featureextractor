package main

import (
	"bufio"
	"fmt"
	_ "io/ioutil"
	"os"
	"sort"
	_ "strconv"
	//"../featureExtractors"
	"strconv"
)

type Pair struct {
	Key   string
	Value float64
}

// A slice of Pairs that implements sort.Interface to sort by Value.
type PairList []Pair

func (p PairList) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p PairList) Len() int           { return len(p) }
func (p PairList) Less(i, j int) bool { return p[i].Key < p[j].Key }

// A function to turn a map into a PairList, then sort and return it.
func sortMapByKey(m map[string]float64) PairList {
	p := make(PairList, len(m))
	i := 0
	for k, v := range m {
		p[i] = Pair{k, v}
		i += 1
	}
	sort.Sort(p)
	return p
}

func FileInput(fileName string) []string {

	file, err := os.Open(fileName)

	if err != nil {
		fmt.Printf("Error opening file %s.\nError message: %s \n", fileName, err)
		return nil
	}

	output, err := readLinesFromFile(file)

	if err != nil {
		fmt.Printf("Error reading file %s.\nError message: %s \n", fileName, err)
		return nil
	}

	return output
}

func readLinesFromFile(file *os.File) ([]string, error) {

	defer file.Close()

	scanner := bufio.NewScanner(file)

	var lines []string

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	return lines, scanner.Err()
}

func FileOutput(fileName string, set FeatureSet) {
	file, err := os.Create(fileName + ".csv") // os.OpenFile(fileName+".csv", os.O_APPEND|os.O_WRONLY, 0600)
	PanicAttack(err)

	defer file.Close()

	featureNames := "screen_name"
	featureValues := set.ScreenName

	// avoid problems with comma symbol in CSV
	if val, ok := set.Features[Punctuation]; ok {
		set.Features["PUNCT"] = val
		delete(set.Features, Punctuation)
	}

	featureList := sortMapByKey(set.Features)

	for i := 0; i < len(featureList); i++ {
		featureNames += "," + featureList[i].Key
		featureValues += "," + strconv.FormatFloat(featureList[i].Value, 'f', -1, 64)
		// fmt.Printf("%s : %s\n", featureList[i].Key, strconv.FormatFloat(featureList[i].Value, 'f', -1, 64))
	}

	_, err = file.WriteString(featureNames + "\n")
	PanicAttack(err)

	_, err = file.WriteString(featureValues + "\n")
	PanicAttack(err)
}

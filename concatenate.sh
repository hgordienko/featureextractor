#!/bin/bash
awk 'FNR==1 && NR!=1{next;}{print}' *.csv >> output.csv
cp output.csv done/new-samples.csv
ls | grep -P ".*.csv" | xargs -d"\n" rm
#Explanation: FNR is the number of lines (records) read so far in the current file. NR is the number of lines read overall. So the condition 'FNR==1 && NR!=1{next;}' says, "Skip this line if it's the first line of the current file, and at least 1 line has been read overall." This has the effect of printing the CSV header of the first file while skipping it in the rest. 

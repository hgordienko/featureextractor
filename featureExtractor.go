//package featureExtractors
package main

import "github.com/ChimeraCoder/anaconda"

type FeatureSet struct {
	ScreenName string
	Features   map[string]float64
}

type RawData struct {
	ScreenName string
	Tweets     []anaconda.Tweet
}

type IFeatureExtractor interface {
	Extract(rawData RawData) <-chan FeatureSet
}

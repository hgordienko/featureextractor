package main

import (
	_ "strings"
	"sync"
)

func PanicAttack(err error) {
	if err != nil {
		panic(err)
	}
}

// func execCommand() {
// 	cmd := exec.Command("/home/lego/Go/bin/ark-tweet-nlp/runTagger.sh", "--output-format", "conll")
// 	cmd.Stdin = strings.NewReader("mama, ama criminal")
// 	var out bytes.Buffer
// 	cmd.Stdout = &out
// 	err := cmd.Run()
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	featureExtr := NewArkTweetNlp()
//
// 	output := regexp.MustCompile("\n").Split(out.String(), -1)
// 	for i, str := range output {
// 		tokenInfo := regexp.MustCompile("\t").Split(str, 3)
// 		if len(tokenInfo) != 3 {
// 			fmt.Println("wooops!")
// 			return
// 		}
//
// 		fmt.Printf("%d:%s|%s|%s \n", i, tokenInfo[0], tokenInfo[1], tokenInfo[2])
// 		confidenceString := strings.Replace(tokenInfo[2], ",", ".", -1)
// 		conf, err := strconv.ParseFloat(confidenceString, 64)
// 		if err != nil {
// 			log.Fatal(err)
// 		}
//
// 		confidence := Float64(conf)
//
// 		if !confidence.isLess(featureExtr.threshold, featureExtr.precision) {
// 			featureExtr.tokensCount[tokenInfo[1]] += 1
// 			featureExtr.tokensTotal += 1
// 		}
// 	}
//
// 	for key, value := range featureExtr.tokensCount {
// 		fmt.Printf("%s %.2f \n", key, value)
// 	}
// }

func merge(cs []<-chan FeatureSet) <-chan FeatureSet {
	var wg sync.WaitGroup
	out := make(chan FeatureSet)
	// Start an output goroutine for each input channel in cs.  output
	// copies values from c to out until c is closed, then calls wg.Done.
	output := func(c <-chan FeatureSet) {
		for n := range c {
			out <- n
		}
		wg.Done()
	}

	wg.Add(len(cs))
	for _, c := range cs {
		go output(c)
	}

	// Start a goroutine to close out once all the output goroutines are
	// done.  This must start after the wg.Add call.
	go func() {
		wg.Wait()
		close(out)
	}()
	return out
}

package main

const (
	Replies  = "RE"
	Retweets = "RT"
	Simple   = "TW"
	Mentions = "MN"
	Hashtags = "HT"
	Media    = "ME"
	Urls     = "URL"
)

type simpleFeatures struct {
	tweetsCount map[string]float64
}

func NewSimpleFeatures() *simpleFeatures {
	featureExtr := simpleFeatures{}
	featureExtr.tweetsCount = map[string]float64{
		Retweets: 0,
		Replies:  0,
		Mentions: 0,
		Simple:   0,
		Hashtags: 0,
		Media:    0,
		Urls:     0,
	}
	return &featureExtr
}

func (featureExtr *simpleFeatures) Extract(data RawData) <-chan FeatureSet {
	out := make(chan FeatureSet)

	go func() {
		// fmt.Printf("SIMPLE_FEATURES: start processing @%s...\n", data.ScreenName)
		for _, tweet := range data.Tweets {
			// fmt.Printf("TEXT\n%s \n", tweet.Text)
			if tweet.RetweetedStatus != nil {
				featureExtr.tweetsCount[Retweets] += 1
				continue
			}
			// fmt.Printf("Reply %s \n", tweet.InReplyToScreenName)
			if tweet.InReplyToScreenName != "" {
				featureExtr.tweetsCount[Replies] += 1
				continue
			}

			featureExtr.tweetsCount[Simple] += 1
			featureExtr.tweetsCount[Mentions] += float64(len(tweet.Entities.User_mentions))
			featureExtr.tweetsCount[Hashtags] += float64(len(tweet.Entities.Hashtags))
			featureExtr.tweetsCount[Media] += float64(len(tweet.Entities.Media))
			featureExtr.tweetsCount[Urls] += float64(len(tweet.Entities.Urls))
		}

		for key, value := range featureExtr.tweetsCount {
			featureExtr.tweetsCount[key] = (value / float64(len(data.Tweets)))
		}

		// fmt.Printf("%s processed, %d tweets \n",
		// 	data.ScreenName,
		// 	len(data.Tweets))

		featureSet := FeatureSet{data.ScreenName, featureExtr.tweetsCount}
		out <- featureSet
		close(out)
	}()
	return out
}
